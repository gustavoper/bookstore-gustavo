<?php

class Book extends Eloquent
{


	public function author(){
		return $this->belongsTo("Author");
	}



	public static function getRules($id= null){

		return array(
			'name' => 'required|min:3|unique:books,name,'.$id
			);
	}

	
}
