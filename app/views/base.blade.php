<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>{{ $title }}</title>

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.4.2/pure-min.css">
    </head>
    <body>
        <div class="pure-g">
            <div class="pure-u-3-4">
                @yield('content')
            </div>
        </div>
    </body>
</html>
