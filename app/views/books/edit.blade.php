@extends('base')

@section('content')
<h1>Edit Author</h1>


@if(Session::has('validation_error'))
<p>{{ Session::get('validation_error') }} </p>
@endif



{{ Form::open(array('route' => 'authors.update', 'class' => 'pure-form pure-form-stacked')) }}

{{ Form::label('name', 'Name') }}
<span style="color: #D55">{{ $errors->first('name') }}</span>
{{ Form::text('name',$author->name) }}

{{ Form::label('bio', 'Biography') }}
{{ Form::text('bio',$author->bio) }}

{{ Form::hidden('id',$author->id) }}


{{ Form::submit('Update', array('class' => 'pure-button pure-button-primary')) }}

{{ Form::close() }}
@stop
