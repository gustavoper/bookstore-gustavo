@extends('base')

@section('content')
<h1>Authors List</h1>
<p>{{ link_to_route('authors.create', 'New Author', array(), array('class' => 'button-success pure-button')) }}</p>


@if(Session::has('author_delete'))
<p>{{ Session::get('author_delete') }} </p>
@endif
@if(Session::has('author_not_found'))
<p>{{ Session::get('author_not_found') }} </p>
@endif



<table class="pure-table pure-table-horizontal">

	<thead>
		<th>Name</th>
		<th colspan="2">Actions</th>
	</thead>

	@foreach ($authors as $author)
	<tr>
		<td>	{{ $author->name }} </td>
		<td>	
			{{ link_to_route('authors.edit', 'Edit', array($author->id), array('class' => 'btn btn-info')) }}
		</td>
		<td>	
			{{ link_to_route('authors.delete', 'Delete', array($author->id), array('class' => 'btn btn-info')) }}
		</td>
	</tr>

	@endforeach
</table>



<h1>Books List</h1>
<p>{{ link_to_route('books.create', 'New Book', array(), array('class' => 'button-success pure-button')) }}</p>
















@stop
