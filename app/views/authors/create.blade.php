@extends('base')

@section('content')
    <h1>New Author</h1>
    {{ Form::open(array('route' => 'authors.save', 'class' => 'pure-form pure-form-stacked')) }}

        {{ Form::label('name', 'Name') }}
        <span style="color: #D55">{{ $errors->first('name') }}</span>
        {{ Form::text('name') }}

        {{ Form::label('bio', 'Biography') }}
        {{ Form::text('bio') }}

        {{ Form::submit('Save', array('class' => 'pure-button pure-button-primary')) }}

    {{ Form::close() }}
@stop
