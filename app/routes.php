<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::get('/authors', 'AuthorsController@getList');


Route::get('/authors/create', array(
    'as' => 'authors.create',
    'uses' => 'AuthorsController@create'
));


Route::post('/authors/save', array(
    'as' => 'authors.save',
    'uses' => 'AuthorsController@save'
));


Route::get('/authors/edit/{id}', array(
    'as' => 'authors.edit',
    'uses' => 'AuthorsController@edit'
));


Route::post('/authors/update', array(
    'as' => 'authors.update',
    'uses' => 'AuthorsController@update'
));


Route::get('/authors/delete/{id}', array(
    'as' => 'authors.delete',
    'uses' => 'AuthorsController@delete'
));




/******

    Criação de livros

******/



Route::get('/books/create', array(
    'as' => 'books.create',
    'uses' => 'BooksController@create'
));


Route::post('/books/save', array(
    'as' => 'books.save',
    'uses' => 'BooksController@save'
));
