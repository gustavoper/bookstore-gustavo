<?php

class AuthorsController extends BaseController {

    public function getList()
    {
        // get authors from database
        $authors = Author::all();

        return View::make('authors.list', array(
            'title' => "Author's list",
            'authors' => $authors
            ));
    }

    public function create()
    {
        return View::make('authors.create', array(
            'title' => 'New author'
            ));
    }

    public function save()
    {
        $data = array(
            'name' => Input::get('name'),
            'bio' => Input::get('bio')
            );

        $validator = Validator::make($data, Author::getRules());

        if (! $validator->fails()) {
            $a = new Author();

            $a->name = $data['name'];
            $a->bio = $data['bio'];

            $a->save();

            return Redirect::to('/authors');
        }

        return Redirect::route('authors.list')->withErrors($validator)->withInput();
        //return Redirect::route('authors.list')->withErrors($validator);
    }




    public function edit($id){
        $author = Author::find($id);
        if (is_null($author))
        {
            //return Redirect::route('authors.edit');
            Session::flash('author_not_found','Not found! =(');
                return Redirect::to('/authors');
            }

            return View::make('authors.edit',  array('title' => "Authors list",'author' => $author));
        }


        public function update(){

            $id = Input::get('id');
            $data = array('name' => Input::get('name'),
                'bio' => Input::get('bio'),
                "id"=>$id);



            $validator = Validator::make($data, Author::getRules($id));

            if (! $validator->fails()) {
                $author = Author::find($id);
                $author->name = $data['name'];
                $author->bio = $data['bio'];
                $author->id = $id;

                $author->save();

                Session::flash("author_update_status","update ok! =)");

                return Redirect::to('/authors');
            }


            return Redirect::route('authors.edit',array('id'=>$id))->withErrors($validator);
        }




        public function delete($id){

            Author::destroy($id);

            Session::flash('author_delete', 'Success!');

            return Redirect::to("/authors");

        }

    }
